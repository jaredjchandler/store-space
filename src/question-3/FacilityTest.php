<?php

declare(strict_types=1);

namespace App\Http\Entities;

use DateTimeImmutable;
use JsonSerializable;
use Helpers\Str;
use PHPUnit\Framework\TestCase;

/**
 * @SWG\Definition()
 */
class FacilityTest extends TestCase implements JsonSerializable
{
    // Test can be created from valid String data
    public function testCreatedwithValidRouteParameters(): void
    {
        $this->assertInstanceOf(
            Facility::class,
            Facility::getRouteParams("Colorado", "Jefferson", "Morrison", "80465")
        );
    }

    // Test cannot be created from non-String data
    public function testCreatedwithInvalidRouteParameters(): void
    {
        $this->assertInstanceOf(
            Facility::class,
            Facility::getRouteParams(4, "twelve", null, "80465")
        );
    }
}
