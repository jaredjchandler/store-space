# What tools/methods would you use to pinpoint the performance bottleneck?
# Refactor the offending method(s) with something more performant.

1. I would review the ORM queries for efficiency. Things like the getAll() payment queries in the controller and the repository layer.
2. In the repository getAll function, Apply the request options default "20" processing limit to the query dumping results into the $payments variable. This could be done using a 'where' clause/filter.
3. Applied proposed changes to improve the getAll performance hit.